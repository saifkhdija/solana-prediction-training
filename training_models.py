from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
import pandas as pd
from joblib import dump
import ta

def prepare_data_for_interval(data, interval_minutes, rsi_window, macd_slow, macd_fast, bollinger_window, atr_window, std_dev_window, momentum_window):

    data['percent_change_prev_close'] = data['close'].pct_change(periods=interval_minutes) * 100
    data['rsi'] = ta.momentum.RSIIndicator(data['close'], window=rsi_window).rsi()
    macd = ta.trend.MACD(data['close'], window_slow=macd_slow, window_fast=macd_fast)
    data['macd'] = macd.macd()
    data['macd_signal'] = macd.macd_signal()
    bollinger = ta.volatility.BollingerBands(close=data['close'], window=bollinger_window, window_dev=2)
    data['bollinger_hband'] = bollinger.bollinger_hband()
    data['bollinger_lband'] = bollinger.bollinger_lband()
    data['bollinger_wband'] = bollinger.bollinger_wband()
    data['atr'] = ta.volatility.AverageTrueRange(close=data['close'], high=data['high'], low=data['low'], window=atr_window).average_true_range()
    data['std_dev_returns'] = data['close'].pct_change().rolling(window=std_dev_window).std()
    data['momentum'] = ta.momentum.ROCIndicator(close=data['close'], window=momentum_window).roc()
    data['percent_change_future_close'] = ((data['close'].shift(-interval_minutes) - data['close']) / data['close']) * 100
    data.dropna(inplace=True)
    return data

df = pd.read_csv('cleaned_dydx_data.csv')

print("training 10 minutes prediction model...")
data = prepare_data_for_interval(
    data=df, 
    interval_minutes=10,
    rsi_window=14, 
    macd_slow=26, 
    macd_fast=12,
    bollinger_window=20, 
    atr_window=14, 
    std_dev_window=20,
    momentum_window=10
)
X = data[['rsi','macd','macd_signal','percent_change_prev_close','bollinger_hband', 'bollinger_lband', 'bollinger_wband', 'atr', 'std_dev_returns', 'momentum']]
y = data['percent_change_future_close']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
model = RandomForestRegressor(n_estimators = 100, random_state=42, max_depth = None, max_features=None, min_samples_split=10)
model.fit(X_train, y_train)
dump(model, 'RF_10_Large.joblib')
print("10 minutes prediction model is ready")

print("training 20 minutes prediction model...")
data = prepare_data_for_interval(
    data=df, 
    interval_minutes=20,
    rsi_window=28,
    macd_slow=52, 
    macd_fast=26, 
    bollinger_window=40,  
    atr_window=28,  
    std_dev_window=40,
    momentum_window=20  
)
X = data[['rsi','macd','macd_signal','percent_change_prev_close','bollinger_hband', 'bollinger_lband', 'bollinger_wband', 'atr', 'std_dev_returns', 'momentum']]
y = data['percent_change_future_close']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
model = RandomForestRegressor(n_estimators = 100, random_state=42, max_depth = None, max_features = None, min_samples_split=10)
model.fit(X_train, y_train)
dump(model, 'RF_20_Large.joblib')
print("20 minutes prediction model is ready")

print("training 30 minutes prediction model...")
data = prepare_data_for_interval(
    data=df, 
    interval_minutes=30,
    rsi_window=42, 
    macd_slow=78, 
    macd_fast=39,
    bollinger_window=60, 
    atr_window=42, 
    std_dev_window=60, 
    momentum_window=30  
)

X = data[['rsi','macd','macd_signal','percent_change_prev_close','bollinger_hband', 'bollinger_lband', 'bollinger_wband', 'atr', 'std_dev_returns', 'momentum']]
y = data['percent_change_future_close']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
model = RandomForestRegressor(n_estimators = 100, random_state=42, max_depth = None, max_features = None, min_samples_split=10)
model.fit(X_train, y_train)
dump(model, 'RF_30_Large.joblib')
print("30 minutes prediction model is ready")

print("training 40 minutes prediction model...")
data = prepare_data_for_interval(
    data=df,  
    interval_minutes=40,
    rsi_window=56, 
    macd_slow=104,  
    macd_fast=52,  
    bollinger_window=80,  
    atr_window=56,  
    std_dev_window=80,  
    momentum_window=40 
)

X = data[['rsi','macd','macd_signal','percent_change_prev_close','bollinger_hband', 'bollinger_lband', 'bollinger_wband', 'atr', 'std_dev_returns', 'momentum']]
y = data['percent_change_future_close']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
model = RandomForestRegressor(n_estimators = 100, random_state=42, max_depth = None, max_features = None, min_samples_split=10)
model.fit(X_train, y_train)
dump(model, 'RF_40_Large.joblib')
print("40 minutes prediction model is ready")

print("training 50 minutes prediction model...")
data = prepare_data_for_interval(
    data=df, 
    interval_minutes=50,
    rsi_window=70,  
    macd_slow=130,  
    macd_fast=65,  
    bollinger_window=100,
    atr_window=70,
    std_dev_window=100,
    momentum_window=50 
)
X = data[['rsi','macd','macd_signal','percent_change_prev_close','bollinger_hband', 'bollinger_lband', 'bollinger_wband', 'atr', 'std_dev_returns', 'momentum']]
y = data['percent_change_future_close']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
model = RandomForestRegressor(n_estimators = 100, random_state=42, max_depth = None,max_features =  None, min_samples_split=10)
model.fit(X_train, y_train)
dump(model, 'RF_50_Large.joblib')
print("50 minutes prediction model is ready")

print("training 60 minutes prediction model...")
prepared_data_60min = prepare_data_for_interval(
    data=df,
    interval_minutes=60,
    rsi_window=84,
    macd_slow=156, 
    macd_fast=78, 
    bollinger_window=120,
    atr_window=84, 
    std_dev_window=120,
    momentum_window=60 
)

X = data[['rsi','macd','macd_signal','percent_change_prev_close','bollinger_hband', 'bollinger_lband', 'bollinger_wband', 'atr', 'std_dev_returns', 'momentum']]
y = data['percent_change_future_close']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)
model = RandomForestRegressor(n_estimators = 100, random_state=42, max_depth = None, max_features = None, min_samples_split=10)
model.fit(X_train, y_train)
dump(model, 'RF_60_Large.joblib')
print("60 minutes prediction model is ready")
